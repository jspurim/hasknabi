module PlayersTest (
  playersTests
) where

import Test.Tasty
import Test.Tasty.HUnit

import Cards
import Clues
import Moves
import Players

playersTests :: TestTree
playersTests = testGroup "Players tests" [polimorphicPlayersTests]

data DifferentPlayer =DifferentPlayer deriving (Show)
instance Player DifferentPlayer where
  makeMove _ = Discard 1

effect = MissPlayAndDraw 1 (ClassicCard Blue Five) Nothing
players = [pack DefaultPlayer, pack (DifferentPlayer)]
moves = map makeMove players
newPlayers =
  map (observeEffect effect) players

polimorphicPlayersTests :: TestTree
polimorphicPlayersTests = testGroup "Polimorphic players" [
  testCase "Polimorphic behaviour" $
    do
      moves @?= [(Play 1),(Discard 1)]
  ]
