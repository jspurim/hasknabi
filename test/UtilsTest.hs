module UtilsTest (
  utilsTests
) where

import System.Random
import Test.Tasty
import Test.Tasty.HUnit

import Cards
import Clues
import Utils
import Variants

utilsTests :: TestTree
utilsTests = testGroup "Utils tests" $ [

  testGroup "Make full clue tests" $ [
    testCase "Rank clue test" $ do
      makeFullClue (RankClue One)
        [ ClassicCard Blue One
        , ClassicCard Green Two
        , ClassicCard Yellow One
        , ClassicCard Red One ] @?= FullRankClue One [True, False, True, True]
    ,
    testCase "Empty rank clue test" $ do
      makeFullClue (RankClue Four)
        [ ClassicCard Blue One
        , ClassicCard Green Two
        , ClassicCard Yellow One
        , ClassicCard Red One ] @?=
          FullRankClue Four [False, False, False, False]
    ,
    testCase "Color clue test" $ do
      makeFullClue (ColorClue Red)
        [ ClassicCard Blue Four
        , ClassicCard Red Two
        , ClassicCard Yellow Five
        , ClassicCard Red Five ] @?=
          FullColorClue Red [False, True, False, True]
    ,
    testCase "Empty color clue test" $ do
      makeFullClue (ColorClue Purple)
        [ ClassicCard Blue Four
        , ClassicCard Red Two
        , ClassicCard Yellow Five
        , ClassicCard Red Five ] @?=
          FullColorClue Purple [False, False, False, False]
    ,
    testCase "Rainbow color clue test" $ do
      makeFullClue (ColorClue Blue)
        [ ClassicCard Blue Four
        , ClassicCard Red Two
        , RainbowCard Five
        , ClassicCard Red Five ] @?=
          FullColorClue Blue [True, False, True, False]
    ],

    testGroup "Shuffle deck tests" [
      testCase "Shuffle changes the deck" $
        let
          deck = generateDeck (ClassicVariant 5)
          gen = mkStdGen 42
          shuffled = shuffleDeck deck gen
        in
          do
            deck /= shuffled @? "Shuffled deck is the same as the unshufled one"
    ,
    testCase "Same seed gives same shuffle" $
      let
        deck = generateDeck (ClassicVariant 5)
        shuffled1 = shuffleDeck deck $ mkStdGen 42
        shuffled2 = shuffleDeck deck $ mkStdGen 42
      in
        do
          shuffled1 == shuffled2 @?
            "Decks ared different! Haskell! You had one job!"
    ,
    testCase "Different seed gives different shuffle" $
      let
        deck = generateDeck (ClassicVariant 5)
        shuffled1 = shuffleDeck deck $ mkStdGen 42
        shuffled2 = shuffleDeck deck $ mkStdGen 43
      in
        do
          shuffled1 /= shuffled2 @? "Both decks are the same"
    ]
    ,
    testGroup "Misc" $ [
      testCase "Hand size for each seats number" $ do
        map handSize [2..5] @?= [5,5,4,4]
    ]
  ]
