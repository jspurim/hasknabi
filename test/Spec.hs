import Test.Tasty
import Test.Tasty.HUnit

import BotTest
import CardsTest
import PlayersTest
import StateTest
import UtilsTest
import VariantsTest

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Hasknabi Tests"
  [cardsTests,
   variantsTests,
   playersTests,
   stateTests,
   botTests,
   utilsTests]
