module CardsTest(
  cardsTests
) where

import Test.Tasty
import Test.Tasty.HUnit

import TestUtils
import Cards
import Clues

assertClueMatch card clue
  = card `matches` clue @? show card ++ " should match " ++ show clue
assertClueNotMatch card clue
  = not (matches card clue) @? show card ++ " should not match " ++ show clue

assertFitPile card pile
  = card `fits` pile @? show card ++ " should fit " ++ show pile
assertNotFitPile card pile
  = not (fits card pile) @? show card ++ " should not fit " ++ show pile

cardsTests :: TestTree
cardsTests = testGroup "Cards tests" [
  classicCardsTests,
  rainbowCardsTests,
  wildRainbowCardsTests,
  whiteCardsTests ]

classicCardsTests :: TestTree
classicCardsTests = testGroup "Classic cards tests"
  [
    testCase "Text representation is correct" $
      let
        expected = [ c ++ "\t" ++ (show n) |
          n <- [1..5],
          c <- ["Blue", "Green", "Yellow", "Red", "Purple", "Orange"]
          ]
        actual = [ show (ClassicCard c n) | n <- enumFrom One, c <- take 6 . enumFrom $ Blue ]
      in
        assertAllEqual expected actual
    ,
    testCase "Blue clue matches all blue cards" $
      do
        assertClueMatch (ClassicCard Blue One) (ColorClue Blue)
        assertClueMatch (ClassicCard Blue Two) (ColorClue Blue)
        assertClueMatch (ClassicCard Blue Three) (ColorClue Blue)
        assertClueMatch (ClassicCard Blue Four) (ColorClue Blue)
        assertClueMatch (ClassicCard Blue Five) (ColorClue Blue)
    ,
    testCase "All fives are matched by the corresponding color clue" $
      do
        assertClueMatch (ClassicCard Blue Five) (ColorClue Blue)
        assertClueMatch (ClassicCard Green Five) (ColorClue Green)
        assertClueMatch (ClassicCard Yellow Five) (ColorClue Yellow)
        assertClueMatch (ClassicCard Red Five) (ColorClue Red)
        assertClueMatch (ClassicCard Purple Five) (ColorClue Purple)
        assertClueMatch (ClassicCard Orange Five) (ColorClue Orange)
    ,
    testCase "Ones clue matches all ones of all colors" $
      do
        assertClueMatch (ClassicCard Blue One) (RankClue One)
        assertClueMatch (ClassicCard Green One) (RankClue One)
        assertClueMatch (ClassicCard Yellow One) (RankClue One)
        assertClueMatch (ClassicCard Red One) (RankClue One)
        assertClueMatch (ClassicCard Purple One) (RankClue One)
        assertClueMatch (ClassicCard Orange One) (RankClue One)
    ,
    testCase "All Orange cards are matched by the corresponing number" $
      do
        assertClueMatch (ClassicCard Orange One) (RankClue One)
        assertClueMatch (ClassicCard Orange Two) (RankClue Two)
        assertClueMatch (ClassicCard Orange Three) (RankClue Three)
        assertClueMatch (ClassicCard Orange Four) (RankClue Four)
        assertClueMatch (ClassicCard Orange Five) (RankClue Five)
    ,
    testCase "From the green cards, twos clue matches ONLY the two" $
      do
        assertClueMatch (ClassicCard Green Two) (RankClue Two)

        assertClueNotMatch (ClassicCard Green One) (RankClue Two)
        assertClueNotMatch (ClassicCard Green Three) (RankClue Two)
        assertClueNotMatch (ClassicCard Green Four) (RankClue Two)
        assertClueNotMatch (ClassicCard Green Five) (RankClue Two)
    ,
    testCase "From all threes, Yellow clue matches ONLY the yellow one" $
      do
        assertClueMatch (ClassicCard Yellow Three) (ColorClue Yellow)

        assertClueNotMatch (ClassicCard Blue Three) (ColorClue Yellow)
        assertClueNotMatch (ClassicCard Green Three) (ColorClue Yellow)
        assertClueNotMatch (ClassicCard Red Three) (ColorClue Yellow)
        assertClueNotMatch (ClassicCard Purple Three) (ColorClue Yellow)
        assertClueNotMatch (ClassicCard Orange Three) (ColorClue Yellow)
    ,
    testCase "Clue match positive smoke test" $
      do
        assertClueMatch (ClassicCard Yellow Four) (ColorClue Yellow)
        assertClueMatch (ClassicCard Purple Two) (ColorClue Purple)
        assertClueMatch (ClassicCard Red One) (ColorClue Red)

        assertClueMatch (ClassicCard Red Two) (RankClue Two)
        assertClueMatch (ClassicCard Blue Four) (RankClue Four)
        assertClueMatch (ClassicCard Orange Five) (RankClue Five)

    ,
    testCase "Clue match negative smoke test" $
      do
        assertClueNotMatch (ClassicCard Blue Five) (ColorClue Yellow)
        assertClueNotMatch (ClassicCard Green One) (ColorClue Blue)
        assertClueNotMatch (ClassicCard Purple Three) (ColorClue Red)

        assertClueNotMatch (ClassicCard Orange Three) (RankClue One)
        assertClueNotMatch (ClassicCard Blue Four) (RankClue Five)
        assertClueNotMatch (ClassicCard Purple Two) (RankClue Four)
    ,
    testCase "All ones fit in the corresponding empty pile" $
      do
        assertFitPile (ClassicCard Blue One) (ClassicPile Blue Nothing)
        assertFitPile (ClassicCard Green One) (ClassicPile Green Nothing)
        assertFitPile (ClassicCard Yellow One) (ClassicPile Yellow Nothing)
        assertFitPile (ClassicCard Red One) (ClassicPile Red Nothing)
        assertFitPile (ClassicCard Purple One) (ClassicPile Purple Nothing)
        assertFitPile (ClassicCard Orange One) (ClassicPile Orange Nothing)
    ,
    testCase "All blue cards fit in the corresponding blue pile" $
      do
        assertFitPile (ClassicCard Blue One) (ClassicPile Blue Nothing)
        assertFitPile (ClassicCard Blue Two) (ClassicPile Blue (Just One))
        assertFitPile (ClassicCard Blue Three) (ClassicPile Blue (Just Two))
        assertFitPile (ClassicCard Blue Four) (ClassicPile Blue (Just Three))
        assertFitPile (ClassicCard Blue Five) (ClassicPile Blue (Just Four))
    ,
    testCase "Green cards wont fit in the red pile" $
      do
        assertNotFitPile (ClassicCard Green One) (ClassicPile Red Nothing)
        assertNotFitPile (ClassicCard Green Two) (ClassicPile Red (Just One))
        assertNotFitPile (ClassicCard Green Three) (ClassicPile Red (Just Two))
        assertNotFitPile (ClassicCard Green Four) (ClassicPile Red (Just Three))
        assertNotFitPile (ClassicCard Green Five) (ClassicPile Red (Just Four))
    ,
    testCase "Cards won't fit into a same color pile until is their turn" $
      do
        assertNotFitPile (ClassicCard Orange Two) (ClassicPile Orange Nothing)
        assertNotFitPile (ClassicCard Orange Three) (ClassicPile Orange (Just One))
        assertNotFitPile (ClassicCard Orange Four) (ClassicPile Orange (Just Two))
        assertNotFitPile (ClassicCard Orange Five) (ClassicPile Orange (Just Three))

        assertNotFitPile (ClassicCard Red Three) (ClassicPile Red Nothing)
        assertNotFitPile (ClassicCard Red Four) (ClassicPile Red (Just One))
        assertNotFitPile (ClassicCard Red Five) (ClassicPile Red (Just Two))

        assertNotFitPile (ClassicCard Yellow Four) (ClassicPile Yellow Nothing)
        assertNotFitPile (ClassicCard Yellow Five) (ClassicPile Yellow (Just One))

        assertNotFitPile (ClassicCard Purple Five) (ClassicPile Purple Nothing)
    ,
    testCase "A pile won't accept cards it already has" $
      do
        assertNotFitPile (ClassicCard Orange One) (ClassicPile Orange (Just Five))
        assertNotFitPile (ClassicCard Orange Two) (ClassicPile Orange (Just Five))
        assertNotFitPile (ClassicCard Orange Three) (ClassicPile Orange (Just Five))
        assertNotFitPile (ClassicCard Orange Four) (ClassicPile Orange (Just Five))
        assertNotFitPile (ClassicCard Orange Five) (ClassicPile Orange (Just Five))
        assertNotFitPile (ClassicCard Purple Two) (ClassicPile Purple (Just Two))

        assertNotFitPile (ClassicCard Red One) (ClassicPile Red (Just Four))
        assertNotFitPile (ClassicCard Red Two) (ClassicPile Red (Just Four))
        assertNotFitPile (ClassicCard Red Three) (ClassicPile Red (Just Four))
        assertNotFitPile (ClassicCard Red Four) (ClassicPile Red (Just Four))

        assertNotFitPile (ClassicCard Blue One) (ClassicPile Blue (Just Three))
        assertNotFitPile (ClassicCard Blue Two) (ClassicPile Blue (Just Three))
        assertNotFitPile (ClassicCard Blue Three) (ClassicPile Blue (Just Three))

        assertNotFitPile (ClassicCard Green One) (ClassicPile Green (Just Two))
        assertNotFitPile (ClassicCard Green Two) (ClassicPile Green (Just Two))

        assertNotFitPile (ClassicCard Purple One) (ClassicPile Purple (Just Two))
        assertNotFitPile (ClassicCard Purple Two) (ClassicPile Purple (Just Two))

        assertNotFitPile (ClassicCard Yellow One) (ClassicPile Yellow (Just One))

  ]

rainbowCardsTests :: TestTree
rainbowCardsTests = testCase "Rainbow cards tests" $ assertFailure "Not implemented!"

wildRainbowCardsTests :: TestTree
wildRainbowCardsTests = testCase "Wild rainbow cards tests" $ assertFailure "Not implemented!"

whiteCardsTests :: TestTree
whiteCardsTests = testCase "White cards tests" $ assertFailure "Not implemented!"
