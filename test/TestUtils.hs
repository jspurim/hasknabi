module TestUtils (
  assertAllEqual,
  rangeSeries,
  rangeListSeries
)  where

import Test.Tasty
import Test.Tasty.HUnit
import Test.SmallCheck.Series

assertAllEqual :: (Eq a, Show a) => [a] -> [a] -> Assertion
assertAllEqual expected actual =
  foldl (>>) (return ()) . map (\(e,a) -> (e @=? a)) $ zip expected actual

rangeSeries s e = generate $ \_ -> [s..e]
rangeListSeries s e l = generate $ (\_ -> sequence $ replicate l [s..e])
