module BotTest (
  botTests
) where

import System.Random

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.SmallCheck
import Test.SmallCheck.Series

import TestUtils

import Cards
import Clues
import Game
import Moves
import Players
import State
import Variants

import Bots.PointingBot
import Bots.SharedKnowledgeBot


gameHistoryProperty playersGen prop =
  over (rangeSeries 3 6) (\colors ->
    over (rangeSeries 0 200) (\seed ->
      let
        initialState = generateInitialState (ClassicVariant colors) (length playersGen) (mkStdGen seed)
        initialGame = makeGame initialState playersGen
        history = playToCompletionWithHistory initialGame
      in
        all prop history
    )
  )

pointingBotsGen = [ \p -> (NamedPlayer "Alice"  (makePointingBot $ p)),
                    \p -> (NamedPlayer "Bob"    (makePointingBot $ p)),
                    \p -> (NamedPlayer "Cathy"  (makePointingBot $ p)),
                    \p -> (NamedPlayer "Dave"   (makePointingBot $ p))]


sharedKnowledgeInvariant ::  (Eq sk) => Game v (SharedKnowledgeBot v sk pk m) -> Bool
sharedKnowledgeInvariant (Game _ players) =
  all (==(head sks)) $ tail sks
  where
    sks = sharedKnowledge . getPlayer <$> players

botTests :: TestTree
botTests = testGroup "Bots tests" [
    testGroup "Shared knowledge invariant" [
      testProperty "Pointing bots" $ gameHistoryProperty pointingBotsGen sharedKnowledgeInvariant
    ]
  ]
