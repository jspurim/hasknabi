module StateTest (
  stateTests
) where

import Data.List
import qualified Data.Map as Map
import System.Random

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.SmallCheck
import Test.SmallCheck.Series

import TestUtils

import Cards
import Clues
import Moves
import Players
import State
import Variants

stateTests :: TestTree
stateTests = testGroup "State tests" $
  [ validMovesTests,
    moveEffectTests,
    initialStateTests,
    stateTransitionTests,
    statePropertiesTests
  ]

hand1 = [ ClassicCard Blue One
        , RainbowWildCard One
        , ClassicCard Red Two
        , ClassicCard Green Three
        , ClassicCard Orange Two]

hand2 = [ ClassicCard Blue Two
        , ClassicCard Purple Four
        , ClassicCard Red Two
        , ClassicCard Orange Four
        , ClassicCard Yellow Five]

exampleHands = [hand1, hand2]

examplePiles = [
    ClassicPile Blue Nothing,
    ClassicPile Green (Just Two),
    ClassicPile Yellow (Just Four),
    ClassicPile Red (Just Five),
    ClassicPile Purple (Just Two),
    ClassicPile Orange Nothing
  ]

validMovesTests :: TestTree
validMovesTests = testGroup "Valid moves tests" $ [
  testCase "Empty clue is illegal" $ do
    not (isValid (GiveClue 1 (ColorClue Green)) 0 exampleHands 5)
      @? "Empty clue allowed"
  ,
  testCase "Self clue is illegal" $ do
    not (isValid (GiveClue 0 (ColorClue Green)) 0 exampleHands 5)
      @? "Self clue allowed"
  ,
  testCase "Clue with no clue tokens is illegal" $ do
    not (isValid (GiveClue 0 (ColorClue Blue)) 1 exampleHands 0)
      @? "Clue allowed without tokens"
  ,
  testCase "Clue to non existent player is illegal" $ do
    not (isValid (GiveClue (-1) (ColorClue Blue)) 1 exampleHands 5)
      @? "Clue allowed to non existent player"

    not (isValid (GiveClue 3 (ColorClue Blue)) 1 exampleHands 5)
      @? "Clue allowed to non existent player"
  ,
  testCase "Discard at 8 clues is illegal" $ do
    not (isValid (Discard 0) 1 exampleHands 8)
      @? "Discard allowed at 8 clues"
  ,
  testCase "Discard non existent card is illegal" $ do
    not (isValid (Discard (-1)) 1 exampleHands 5)
      @? "Discard allowed for non existent card"
    not (isValid (Discard 5) 1 exampleHands 5)
      @? "Discard allowed for non existent card"
  ,
  testCase "Play non existent card is illegal" $ do
    not (isValid (Play (-1)) 1 exampleHands 5)
      @? "Play allowed for non existent card"
    not (isValid (Play 5) 1 exampleHands 5)
      @? "Play allowed for non existent card"
  ,
  testCase "Correct plays are allowed" $ do
    (isValid (Play 0) 1 exampleHands 5)
      @? "Correct play disallowed"
    (isValid (Play 2) 2 exampleHands 0)
      @? "Correct play disallowed"
  ,
  testCase "Correct discards are allowed" $ do
    (isValid (Discard 0) 1 exampleHands 5)
      @? "Correct discard disallowed"
    (isValid (Discard 2) 2 exampleHands 0)
      @? "Correct discard disallowed"
  ,
  testCase "Correct discards are allowed" $ do
    (isValid (Discard 0) 1 exampleHands 5)
      @? "Correct discard disallowed"
    (isValid (Discard 2) 2 exampleHands 0)
      @? "Correct discard disallowed"
  ,
  testCase "Correct clues are allowed" $ do
    (isValid (GiveClue 1 (ColorClue Red)) 0 exampleHands 5)
      @? "Correct clue disallowed"
    (isValid (GiveClue 0 (RankClue One)) 1 exampleHands 5)
      @? "Correct clue disallowed"
  ]

moveEffectTests :: TestTree
moveEffectTests = testGroup "Move effect tests" $ [
  testCase "Compute play effect (success)" $
    do
      calculateMoveEffect 0 exampleHands examplePiles
        (Just (ClassicCard Blue Five)) (Play 0) @?=
          (PlayAndDraw 0 (ClassicCard Blue One) 0 (Just(ClassicCard Blue Five)))
  ,
  testCase "Compute play effect (missplay)" $
    do
      calculateMoveEffect 1 exampleHands examplePiles
        (Just (ClassicCard Blue Five)) (Play 0) @?=
          (MissPlayAndDraw 0 (ClassicCard Blue Two)
            (Just(ClassicCard Blue Five)))
  ,
  testCase "Compute discard effect" $
    do
      calculateMoveEffect 1 exampleHands examplePiles
        (Just (ClassicCard Blue Five)) (Discard 0) @?=
          (DiscardAndDraw 0 (ClassicCard Blue Two)
            (Just(ClassicCard Blue Five)))
  ,
  testCase "Compute give clue effect" $
    do
      calculateMoveEffect 1 exampleHands examplePiles
        (Just (ClassicCard Blue Five))
          (GiveClue 0 (ColorClue Green)) @?=
          (ClueGiven 0 (FullColorClue Green [False, True, False, True, False]))
  ]

initialStateProperty p =
  over (rangeSeries 2 5) $ \seats ->
  over (rangeSeries 3 6) $ \suits seed ->
    p seats suits seed $ generateInitialState (ClassicVariant suits) seats (mkStdGen seed)

initialStateTests :: TestTree
initialStateTests = testGroup "Initial state tests" $ [
  testProperty "Dealt hands plus remaining deck equal full deck" $
    initialStateProperty $ \_ _ _ state ->
      let
        dealtCards = concat . hands $ state
        tableDeck = sort $ dealtCards ++ (deck state)
        expectedDeck = sort . generateDeck . variant . publicState $ state
      in
        tableDeck == expectedDeck
  ,
  testProperty "Number of piles is correct and they are empty" $
    initialStateProperty $ \_ suits _ state ->
      let
        p = piles . publicState $ state
      in
        suits == (length p) &&
        all ((==) (Just One) . nextRank)  p
  ,
  testProperty "Number of seats is correct and matches hands" $
    initialStateProperty $ \seatsNumber _ _ state ->
      let
        actualSeats = seats . publicState $ state
        numberOfHands = length . hands $ state
      in
        seatsNumber == actualSeats && seatsNumber == numberOfHands
  ,
  testProperty "Variant is correct" $
    initialStateProperty $ \_ suits _ state ->
      variant (publicState state) == ClassicVariant suits
  ,
  testProperty "Clues at 8, strikes at 0 " $
    initialStateProperty $ \_ _ _ state ->
      let
        c = clues . publicState $ state
        s = strikes . publicState $ state
      in
        (c,s) == (8,0)
  ,
  testProperty "Discard is empty" $
    initialStateProperty $ \_ _ _ state ->
      let
        cards = uniqueCards . variant . publicState $ state
        disc = discard . publicState $ state
      in
        all (((==) (Just 0)) . (flip Map.lookup) disc)  cards
  ,
  testProperty "Turn is 0" $
    initialStateProperty $ \_ _ _ state ->
      (turn . publicState) state == 0
  ,
  testProperty "History is empty" $
    initialStateProperty $ \_ _ _ state ->
      (history . publicState) state == []

  ]

statePropertiesTests :: TestTree
statePropertiesTests = testGroup "State properties tests" [
    testProperty "Score test" $ over (rangeListSeries 0 5 5) $ \scores ->
      map pileScore (zipWith ClassicPile [Blue .. Purple] (maybeRankFromInt <$> scores))
        == scores
  ]

stateTransitionTests :: TestTree
stateTransitionTests = testCase "State transition tests" $
  assertFailure "Not implemented!"
