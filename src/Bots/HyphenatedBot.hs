module Bots.HyphenatedBot (

) where

import Cards
import Clues
import Moves
import State
import Utils
import Variants
import Bots.Common
import Bots.SharedKnowledgeBot

data HBCardInfo = HBCardInfo {
  clued :: Bool,
  posibleSuits :: [Suit],
  posibleRanks :: [Rank],
  order :: Int,
  cardSeat :: Int,
  slot :: Int,
  chopMoved :: Bool
}

type HBPrivateKnowledge = ()

possibleCards :: HBCardInfo -> [Card]
possibleCards info =
  makeCard <$> (posibleSuits info) <*> (posibleRanks info)

possibleCardsSK :: Int -> Int -> HBSharedKnowledge -> [Card]
possibleCardsSK s p sk = possibleCards $  (handsInfo sk)!!s!!p

possibleCardsPerspective :: Variant v => Int -> Int -> Perspective v -> HBSharedKnowledge -> [Card]
possibleCardsPerspective s p perspective sk =
  if s /= seat perspective then
    [(partialHands perspective)!!s!!p]
  else
    [c | c <- possibleCards info, not $ elem  c $ accountedForCards perspective]
  where
    info = (handsInfo sk)!!s!!p

type HBHandInfo = [HBCardInfo]

chop :: HBHandInfo -> Int
chop handInfo =
  case candidates of
    [] -> length handInfo - 1
    xs -> maximum xs
  where
  candidates = fst <$> (filter (\(i,info) -> not (clued info || chopMoved info)) $ enumerated handInfo)

data Promise = Promise {
    who :: Int,
    promisedCard :: Card,
    demonstrated :: Bool,
    priorities :: [Int]
  }

data  HBSharedKnowledge = HBSharedKnowledge {
    handsInfo :: [HBHandInfo],
    promises :: [Promise]
  }

hbPosibleEffects :: (Perspective v) -> HBSharedKnowledge -> () -> Move -> [MoveEffect]
hbPosibleEffects perspective sk pk move =
  case move of
    GiveClue p clue -> [ClueGiven p $ makeFullClue clue $ (partialHands perspective)!!p]
    Discard p -> (\c -> DiscardAndDraw p c (Just UnknownCard)) <$> (possibleCardsSK s p sk)
    Play p -> calculatePlayCardEffect (piles state) p <$> (possibleCardsSK s p sk)
    where
      state = visibleState perspective
      s = seat perspective

hbInference :: (Perspective v) -> HBPrivateKnowledge -> HBSharedKnowledge -> HBPrivateKnowledge
hbInference _ _ _ = ()

hbValuation :: (Perspective v) -> HBPrivateKnowledge -> HBSharedKnowledge -> Int
hbValuation perspective _ sk =
  ((score . visibleState $ perspective) * 20) +
  (strikes . visibleState $ perspective) * (-100) +
  gtpBonus * 20 +
  gottenCardsBonus +
  playableCardsBonus +
  criticalChopPenalty
  where
    hands = partialHands perspective
    gottenCards = filter clued $ foldl (++) [] (handsInfo sk)
    gtpBonus=1
    criticalChopPenalty = -10
    playableCardsBonus = 10
    gottenCardsBonus = 10
