module Bots.Common (
  playableCards,
  playableCardsWithIndex,
  calculatePlayCardEffect,
  accountedForCards,
) where

import Cards
import Moves
import State
import Variants



playableCardsWithIndex :: [Pile] -> [(Int,Card)]
playableCardsWithIndex piles = [(i,x) | (i,Just x) <- zip [0..] (map nextCard piles)]

playableCards :: [Pile] -> [Card]
playableCards piles = snd <$> (playableCardsWithIndex  piles)

calculatePlayCardEffect :: [Pile] -> Int -> Card -> MoveEffect
calculatePlayCardEffect piles p card =
  case filter (\(_,c) -> c == card) $ playableCardsWithIndex piles of
    [] -> MissPlayAndDraw p card (Just UnknownCard)
    ((i,_):_) -> PlayAndDraw p card i (Just UnknownCard)

accountedForCards :: Variant v => Perspective v -> [Card]
accountedForCards perspective =
  allOutOfPlay d' $ variant . visibleState $ perspective
  where
    s = seat perspective
    d = discard . visibleState $ perspective
    othersHands = [h | (i,h) <- zip [0..]  (partialHands perspective), i /= s]
    cardsInView = foldl (++) []  othersHands
    d' = discardAll d cardsInView
