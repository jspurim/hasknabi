{-# Language InstanceSigs #-}

module Bots.PointingBot (
  makePointingBot
) where

import qualified Data.Map as Map
import qualified Data.List as List
import qualified Debug.Trace as Trace
import Bots.SharedKnowledgeBot

import Bots.Common
import Cards
import Clues
import Moves
import Players
import State
import Utils

clueMeanings = Map.fromList [
    (ColorClue Blue, 0),
    (ColorClue Green, 1),
    (ColorClue Yellow, 2),
    (ColorClue Red, 3),
    (ColorClue Purple, 4),
    (ColorClue Orange, 3),
    (RankClue One, 0),
    (RankClue Two, 1),
    (RankClue Three, 2),
    (RankClue Four, 3),
    (RankClue Five, 4)
  ]

allClues :: [Clue]
allClues =  (map ColorClue $ enumFrom Blue) ++ (map RankClue $ enumFrom One)

clueMeaning :: Clue -> Int -> Int
clueMeaning clue handSize =
  Map.findWithDefault 0 clue clueMeanings `mod` handSize

type Marks = [(Int,Int)]

validClue:: Int -> [Hand] -> Marks -> PublicState a-> Maybe Move
validClue seat hands marks state =
  case clues state of
    0 -> Nothing
    _ -> case [(p, clue) | clue <- neededClues, p <- otherSeats,
      not . isEmpty $ makeFullClue clue $ (hands!!p)] of
        ((p,clue):_) -> Just (GiveClue p clue)
        [] -> Nothing
  where
    handSize = length (hands!!seat)
    otherSeats = [s | s <- [0..(length hands-1)], s /= seat]
    nextSeat = (seat+1) `mod` (length hands)
    nextHand = [(p,c) | (p, c) <- zip [0..] (hands!!nextSeat), c /= UnknownCard]
    marked = [c | c <- map (\(s,p) -> (hands!!s)!!p) marks, c /= UnknownCard]
    playable = playableCards . piles $ state
    markable = [p | (p,c) <- nextHand, elem c playable, not (elem c marked)]
    neededClues = [clue | clue <- allClues, (clueMeaning clue handSize) `elem` markable]

rotateMarks marks p t  =
  affected' ++ notAffected
  where
    affected = [(s,p') | (s,p') <- marks, s==t]
    notAffected = [(s,p') | (s,p') <- marks, s/=t]
    affected' = [(s,if p' > p then p'-1 else p') | (s, p') <- affected, p/=p' ]


pointingBotInterpretation :: Marks -> MoveEffect -> Perspective v -> Maybe Marks
pointingBotInterpretation marks effect perspective =
   case effect of
    ClueGiven p fullClue ->
      let
        np = clueMeaning (originalClue fullClue) hs
      in
        Just ((nt,np):marks)
    DiscardAndDraw p _ _ -> Just(rotateMarks marks p t)
    PlayAndDraw p _ _ _ -> Just(rotateMarks marks p t)
    MissPlayAndDraw p _ _ -> Just(rotateMarks marks p t)
  where
    state = visibleState perspective
    t = turn state
    nt = (t+1) `mod` (length $ partialHands perspective)
    hs = handSize (length $ partialHands perspective)

pointingBotInference :: Perspective v -> () -> Marks -> ()
pointingBotInference _ _ _ = ()

pointingBotPossibleEffects :: (Perspective v) -> Marks -> () -> Move -> [MoveEffect]
pointingBotPossibleEffects perspective marks _ move =
  case move of
    GiveClue p clue -> [ClueGiven p $ makeFullClue clue $ (partialHands perspective)!!p]
    Discard p -> [DiscardAndDraw p UnknownCard (Just UnknownCard)]
    Play p ->
      if (s,p) `elem` marks then
        successfulPlays p
      else
        (MissPlayAndDraw p UnknownCard (Just UnknownCard)):(successfulPlays p)
    where
      s = seat perspective
      successfulPlays p = (\(pile, c) -> (PlayAndDraw p c pile (Just UnknownCard))) <$>
        ((playableCardsWithIndex . piles $ visibleState perspective))

pointingBotValuation :: (Perspective v) -> () -> Marks -> Int
pointingBotValuation perspective _ marks =
  ((score . visibleState $ perspective)*10) +
  sum ((\(s,p) -> if  (p < length (hands!!s)) && (hands!!s!!p `elem`
    (playableCards . piles . visibleState $ perspective)) then 1 else -50) <$> marks) -
  (strikes . visibleState $ perspective)*100
  where
    hands = partialHands perspective


makePointingBot :: Perspective v -> (SharedKnowledgeBot v Marks () Int)
makePointingBot perspective =
  SharedKnowledgeBot{
    perspective=perspective,
    sharedKnowledge=[],
    privateKnowledge=(),
    inference=pointingBotInference,
    valuation=pointingBotValuation,
    interpretation=pointingBotInterpretation,
    possibleEffects=pointingBotPossibleEffects
  }
