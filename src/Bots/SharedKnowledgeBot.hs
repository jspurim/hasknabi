module Bots.SharedKnowledgeBot(
  SharedKnowledgeBot(..)
) where

import qualified Data.List as List
import qualified Debug.Trace as Trace

import Clues
import Moves
import Players
import State
import Utils
import Variants

type Line = [Move]

class PrivateKnowledge pk where
  nullKnowledge :: pk

instance PrivateKnowledge () where
  nullKnowledge = ()

data SharedKnowledgeBot v sk pk m = SharedKnowledgeBot {
  perspective :: Perspective v,
  sharedKnowledge :: sk,
  privateKnowledge :: pk,
  valuation :: (Perspective v) -> pk -> sk -> m,
  inference :: (Perspective v) -> pk -> sk -> pk,
  interpretation :: sk -> MoveEffect -> (Perspective v) -> (Maybe sk),
  possibleEffects :: (Perspective v) -> sk -> pk -> Move -> [MoveEffect]
}

instance (Show sk, Variant v) => Show (SharedKnowledgeBot v sk pk m)  where
  show player =
    "Shared knowledge: " ++ (show . sharedKnowledge $ player) ++
    "\nPerspective: " ++ (show . perspective $ player)

condenseIfEqual :: Eq a => [Maybe a] -> Maybe a
condenseIfEqual [] = Nothing
condenseIfEqual (x:xs) = case all (==x) xs of
  True -> x
  False -> Nothing

possibleMoves :: Variant v => Perspective v -> [Move]
possibleMoves Perspective{seat=seat, visibleState=state, partialHands=hands} =
  filter (\m -> isValid m (turn state) hands (clues state))
    (plays ++ discards ++ possibleClues)
  where
    handPos = [0..((handSize . seats $ state)-1)]
    plays = Play <$> handPos
    discards = Discard <$> handPos
    others = filter (/=(turn state)) [0..((seats state)-1)]
    colorClues = ColorClue <$> (clueColors . variant $ state)
    rankClues = RankClue <$> (enumFrom One)
    possibleClues = GiveClue <$> others <*>  (rankClues ++ colorClues)

chooseBestMove:: (Eq sk, Variant v, Ord m, Show m) => (SharedKnowledgeBot v sk pk m) -> Move
chooseBestMove bot@SharedKnowledgeBot {
  perspective=perspective@Perspective{
    seat=seat,
    partialHands=hands,
    visibleState=state
  },
  sharedKnowledge=sharedKnowledge,
  privateKnowledge=privateKnowledge,
  valuation=valuation,
  inference=_,
  interpretation=interpretation,
  possibleEffects=possibleEffects
} = bestMove
  where
    others = [0..((seats state)-1)]
    othersPerspectives = [blindTo x perspective | x <- others]
    calculateSks = (\me -> condenseIfEqual $ interpretation sharedKnowledge me <$> othersPerspectives)
    options :: [[MoveEffect]]
    options = possibleEffects perspective sharedKnowledge  privateKnowledge <$> (possibleMoves perspective)
    optionsSks =  (\mes -> zip mes (calculateSks <$> mes) ) <$> options
    filteredOptionsSks = filter (\mes -> all (\(_,k) -> k/= Nothing) mes) optionsSks
    unpackedOptions = (\mes ->[(me,k) | (me, Just k) <- mes]) <$> filteredOptionsSks
    evaluatedOptions =
      (\mes -> (\(me, sk ) -> (valuation (applyEffectOnPerspective me perspective) privateKnowledge sk, me)) <$> mes) <$> unpackedOptions
    condensedOptions = (\mes -> (minimum (fst <$> mes), recoverMove . snd . head $ mes)) <$> evaluatedOptions

    bestMove = --Trace.trace ("Options considered:\n\t" ++ List.intercalate "\n\t" (show <$> List.sort condensedOptions))
      snd . last . List.sort $ condensedOptions


instance (Eq sk, Show sk, Show m, Ord m, PrivateKnowledge pk, Variant v) => Player (SharedKnowledgeBot v sk pk m) where
  makeMove sharedKnowledgeBot = chooseBestMove sharedKnowledgeBot

  observeEffect effect bot@SharedKnowledgeBot{
      perspective=perspective@Perspective{
        seat=seat,
        partialHands=hands,
        visibleState=state
      },
      sharedKnowledge=sharedKnowledge,
      privateKnowledge=privateKnowledge,
      valuation=valuation,
      inference=inference,
      interpretation=interpretation,
      possibleEffects=possibleEffects
    } =
      bot{
        perspective=newPerspective,
        sharedKnowledge=newSharedKnowledge,
        privateKnowledge=newPrivateKnowledge
      }
    where
      Just newSharedKnowledge =
        if turn state /= seat then
          interpretation sharedKnowledge effect perspective
          else interpretation sharedKnowledge effect (nextPlayerPerspective perspective)
      newPerspective = applyEffectOnPerspective effect perspective
      newPrivateKnowledge = inference newPerspective privateKnowledge newSharedKnowledge
