module Utils (
  makeFullClue,
  shuffleDeck,
  handSize,
  unpackMaybes,
  enumerated
) where

import System.Random
import System.Random.Shuffle

import Cards
import Clues

makeFullClue :: Clue -> Hand -> FullClue
makeFullClue clue@(ColorClue color) hand =
  FullColorClue color $ map  (flip matches clue) $ hand
makeFullClue clue@(RankClue rank) hand =
  FullRankClue rank $ map  (flip matches clue) $ hand


shuffleDeck :: RandomGen gen  => [Card] -> gen -> [Card]
shuffleDeck deck gen = shuffle' deck (length deck) gen

handSize :: Int -> Int
handSize seats = if seats <= 3 then 5 else 4

unpackMaybes :: [Maybe a] -> [a]
unpackMaybes ls = [x | Just x <- ls]

enumerated :: [a] -> [(Int,a)]
enumerated xs = zip [0..] xs
