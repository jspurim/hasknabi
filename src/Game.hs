module Game (
  Game(..),
  makeGame,
  playOneTurn,
  playToCompletion,
  playToCompletionWithHistory,
  gameFinished
) where

import Data.List
import Data.List.Split

import Cards
import Moves
import Players
import State
import Variants

data Game a b =  Game (FullState a) [NamedPlayer b]

makeGame :: Variant a => FullState a -> [(Perspective a) -> NamedPlayer b] -> Game a b
makeGame state playerInitializers =
  Game state players
  where
    players = zipWith (\f x -> f x) playerInitializers
      [makePerspective state i | i <- [0..]]

playOneTurn :: (Variant a, Player b) => Game a b -> Game a b
playOneTurn (Game state players) =
  Game newState newPlayers
  where
    currentPlayerSeat = turn . publicState $ state
    currentPlayer = (!!) players . turn . publicState $ state
    move = makeMove currentPlayer
    moveEffect = calculateMoveEffectFromState state move
    observedEffect =
      [if p == currentPlayerSeat then censorEffect moveEffect else moveEffect |
        p <- [0..(length players)]]
    newPlayers = zipWith observeEffect observedEffect players
    newState = applyEffectFull moveEffect state


playToCompletion :: (Variant a, Player b) => Game a b -> Game a b
playToCompletion game@(Game state players) =
  if gameEnded state then game else playToCompletion $ playOneTurn  game

playToCompletionWithHistory :: (Variant a, Player b) => Game a b -> [Game a b]
playToCompletionWithHistory game@(Game state players) =
  if gameEnded state then
    [game]
  else
    (game:(playToCompletionWithHistory $ playOneTurn  game))

gameFinished :: (Variant a, Player b) => Game a b -> Bool
gameFinished (Game state _) =
  gameEnded state

instance (Variant a, Player b) => Show (Game a b) where
  show (Game state players) =
    "Turn " ++ show (turnsPlayed pstate) ++"\t\t" ++ toPlay
    ++ header
    ++ "\n" ++ pilesRepr ++ "\n\n" ++ handsRepr
    ++ "\n\nDiscard: \n" ++ discardRpr

    where
      pstate = publicState $ state
      header = "\t\t\t\t\t\t\t\tClues: "
        ++ (show $ clues pstate)
        ++ "\n\t\t\t\t\t\t\t\tDeck: "  ++ show (deckSize pstate)
        ++ "\n\t\t\t\t\t\t\t\tStrikes: "
        ++ (take 3 $ replicate (strikes pstate) 'X' ++ "___")
        ++ "\n\t\t\t\t\t\t\t\tScore: " ++ show (score pstate)
        ++ lastTurnRepr
      pilesRepr = intercalate "\n" $ map show $ piles $ pstate
      handsRepr = intercalate "\n" [name player ++ ": \t\t" ++ showHand hand
        | (hand, player) <- zip (hands state)  players]
      discardedCards = listDiscardedCards pstate
      discardRpr = intercalate "\n" $ map (\x -> intercalate ", " $ map show' x )
        $ chunksOf 6 discardedCards
      toPlay = case (gameEnded state) of
        False -> name (players !! (turn pstate) ) ++ " to play...\n"
        True -> "Game ended.\n"
      lastTurnRepr =  case lastTurn pstate of
        Nothing -> ""
        Just x -> "\n\t\t\t\t\t\t\t\tLast turn: "++show x++"\n"
