module Moves(
  Move(..),
  MoveEffect(..),
  censorEffect,
  recoverMove,
  showMove
) where

import Clues
import Cards

data Move = Discard Int
          | Play Int
          | GiveClue Int Clue
          deriving (Eq, Show, Ord)

data MoveEffect = DiscardAndDraw Int Card (Maybe Card)
                | PlayAndDraw Int Card Int (Maybe Card)
                | ClueGiven Int FullClue
                | MissPlayAndDraw Int Card (Maybe Card)
                deriving (Eq, Show)

recoverMove :: MoveEffect -> Move
recoverMove (DiscardAndDraw p _ _) = Discard p
recoverMove (PlayAndDraw p _ _ _) = Play p
recoverMove (MissPlayAndDraw p _ _) = Play p
recoverMove (ClueGiven p (FullColorClue c _)) = GiveClue p (ColorClue c)
recoverMove (ClueGiven p (FullRankClue n _)) = GiveClue p (RankClue n)

censorEffect :: MoveEffect -> MoveEffect
censorEffect (DiscardAndDraw p c (Just card)) = DiscardAndDraw p c (Just UnknownCard)
censorEffect (PlayAndDraw p c pile (Just card)) = PlayAndDraw p c pile (Just UnknownCard)
censorEffect (MissPlayAndDraw p c (Just card)) = MissPlayAndDraw p c (Just UnknownCard)
censorEffect e = e

showMove :: String -> [String] -> [String] -> MoveEffect -> String
showMove name names piles (DiscardAndDraw slot discarded drawn) =
  name ++ " discards " ++ show' discarded ++ " from slot #" ++ show slot ++
    " and draws " ++ showMaybeCard drawn
showMove name names piles (PlayAndDraw slot played pile drawn) =
  name ++ " successfully plays " ++ show' played ++ " from slot #" ++ show slot ++
    " into " ++ (piles !! pile) ++ " pile and draws " ++ showMaybeCard drawn
showMove name names piles (MissPlayAndDraw slot played drawn) =
  name ++ " fails to play " ++ show' played ++ " from slot #" ++ show slot ++
    " and draws " ++ showMaybeCard drawn
showMove name names piles (ClueGiven p clue ) =
    name ++ " clues " ++ clueStr ++ " to " ++ (names !! p)
  where
    clueStr = case clue of
      (FullRankClue rank _) -> show rank ++ "s"
      (FullColorClue color _) -> show color ++ "s"
