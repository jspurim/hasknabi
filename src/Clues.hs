module Clues (
  Clue (..),
  FullClue (..),
  Color(..),
  Rank(..),
  matched,
  rankToInt,
  rankFromInt,
  maybeRankFromInt,
  isEmpty,
  originalClue
) where

data Color = Blue | Green | Yellow | Red | Purple | Orange
  deriving (Eq, Show, Enum, Ord)

data Rank = One | Two | Three | Four | Five
  deriving (Eq, Enum, Ord)

instance Show Rank where
  show One = "1"
  show Two = "2"
  show Three = "3"
  show Four = "4"
  show Five = "5"

rankFromInt :: Int -> Rank
rankFromInt n = toEnum (n-1)

maybeRankFromInt :: Int -> Maybe Rank
maybeRankFromInt 0 = Nothing
maybeRankFromInt n = Just (toEnum (n-1))

rankToInt :: Rank -> Int
rankToInt r = (fromEnum r) + 1

data Clue = ColorClue Color | RankClue Rank deriving (Show, Eq, Ord)
data FullClue = FullColorClue Color [Bool] | FullRankClue Rank [Bool]
  deriving (Show, Eq)

originalClue :: FullClue -> Clue
originalClue (FullColorClue c _) = ColorClue c
originalClue (FullRankClue n _) = RankClue n

matched :: FullClue -> [Bool]
matched (FullColorClue _ m) = m
matched (FullRankClue _ m) = m

isEmpty :: FullClue -> Bool
isEmpty clue = all not $ matched clue
