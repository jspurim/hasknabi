{-# LANGUAGE ExistentialQuantification #-}

module Players (
  Player (..),
  DefaultPlayer (..),
  NamedPlayer(..),
  name,
  getPlayer,
  pack
) where

import Cards
import Clues
import Moves

class Show a => Player a where
  makeMove :: a -> Move
  observeEffect :: MoveEffect -> a -> a
  makeMove player = Play 1
  observeEffect _ = id

data DefaultPlayer = DefaultPlayer deriving (Show)
instance Player DefaultPlayer

data NamedPlayer p= NamedPlayer String p
instance (Player p) => Player (NamedPlayer p) where
  makeMove (NamedPlayer x p) = makeMove p
  observeEffect effect (NamedPlayer x p) = (NamedPlayer x (observeEffect effect p))

instance (Show p) => Show (NamedPlayer p) where
  show (NamedPlayer x p) = x ++ ":\n"++show p

name :: NamedPlayer p -> String
name (NamedPlayer x _) = x

getPlayer :: NamedPlayer p -> p
getPlayer (NamedPlayer _ p) = p

data Playable = forall a . Player a => MkPlayable a
pack :: Player a => a -> Playable
pack = MkPlayable

instance Show Playable where
  show (MkPlayable player) = show player

instance Player Playable where
  makeMove (MkPlayable player) = makeMove  player
  observeEffect effect (MkPlayable player) = pack $ observeEffect effect player
