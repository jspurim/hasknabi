module Cards (
  Card (..),
  rank,
  matches,
  fits,
  show',
  showMaybeCard,
  Hand,
  showHand,
  Suit (..),
  clueableColors,
  makeCard,
  makePile,
  Pile (..),
  nextRank,
  nextCard,
  increase,
  pileScore,
  Discard,
  discardCard,
  discardAll
) where

import qualified Data.Map as Map
import Data.List

import Clues

data Suit = ClassicSuit Color
  | RainbowSuit
  | WhiteSuit
  deriving (Eq)

instance Show Suit where
  show (ClassicSuit color) = show color
  show RainbowSuit = "Rainbow"
  show WhiteSuit = "White"

clueableColors :: Suit -> [Color]
clueableColors (ClassicSuit c) = [c]
clueableColors _ = []

makeCard :: Suit -> Rank -> Card
makeCard (ClassicSuit c) = ClassicCard c
makeCard RainbowSuit = RainbowCard
makeCard WhiteSuit = WhiteCard

makePile :: Suit -> Pile
makePile (ClassicSuit c) = ClassicPile c Nothing
makePile RainbowSuit = RainbowPile Nothing
makePile WhiteSuit = WhitePile Nothing

data Card = ClassicCard Color Rank
  | RainbowCard Rank
  | RainbowWildCard Rank
  | WhiteCard Rank
  | UnknownCard
  | BlackedOutCard
  deriving (Eq, Ord)

rank :: Card -> Rank
rank (ClassicCard _ n) = n
rank (RainbowCard n) = n
rank (RainbowWildCard n) =  n
rank (WhiteCard n) = n

matches :: Card -> Clue -> Bool
matches card (RankClue n) = rank card == n
matches (ClassicCard c n) (ColorClue c') = c == c'
matches (RainbowCard n) (ColorClue _) = True
matches (RainbowWildCard n) (ColorClue _) = True
matches UnknownCard _ = True
matches BlackedOutCard _ = False
matches _ _ = False


fits :: Card -> Pile -> Bool
fits (RainbowWildCard n) (pile) = nextRank pile == Just n
fits (ClassicCard c n)  pile@(ClassicPile c' _)
  = if c == c' then (nextRank pile == (Just n)) else False
fits (RainbowCard n) pile@(RainbowPile _) = nextRank pile == Just n
fits (WhiteCard n) pile@(WhitePile n') = nextRank pile == Just n
fits UnknownCard _ = False
fits BlackedOutCard _ = False
fits _ _ = False

instance Show Card where
  show (ClassicCard c n) =  show c ++ "\t" ++ show n
  show (RainbowCard n) = "Rainbow \t" ++ show n
  show (RainbowWildCard n) = "(W)Rainbow \t" ++ show n
  show (WhiteCard n) = "White \t" ++ show n
  show UnknownCard = "Unknown card"
  show BlackedOutCard  = "Blacked out card"

show' :: Card -> String
show' card = map (\x -> if x == '\t' then ' ' else x) $ show card

showMaybeCard :: Maybe Card -> String
showMaybeCard Nothing = "Nothing"
showMaybeCard (Just c) = show' c


type Hand = [Card]
showHand :: Hand -> String
showHand hand =
  intercalate "\t" $ map show $ hand

data Pile = ClassicPile Color (Maybe Rank)
  | RainbowPile (Maybe Rank)
  | WhitePile (Maybe Rank)

nextRank :: Pile -> Maybe Rank
nextRank (ClassicPile _ (Just Five)) = Nothing
nextRank (ClassicPile _ (Just n)) = Just (succ n)
nextRank (ClassicPile _ Nothing) = Just One

nextRank (RainbowPile (Just Five)) = Nothing
nextRank (RainbowPile (Just n)) = Just (succ n)
nextRank (RainbowPile Nothing) = Just One

nextRank (WhitePile (Just Five)) = Nothing
nextRank (WhitePile (Just n)) = Just (succ n)
nextRank (WhitePile Nothing) = Just One

nextCard :: Pile -> Maybe Card
nextCard (ClassicPile _ (Just Five)) = Nothing
nextCard (ClassicPile c (Just n)) = Just (ClassicCard c (succ n))
nextCard (ClassicPile c Nothing) = Just (ClassicCard c One)

nextCard (RainbowPile (Just Five)) = Nothing
nextCard (RainbowPile (Just n)) = Just (RainbowCard (succ n))
nextCard (RainbowPile Nothing) = Just (RainbowCard One)

nextCard (WhitePile (Just Five)) = Nothing
nextCard (WhitePile (Just n)) = Just (WhiteCard (succ n))
nextCard (WhitePile Nothing) = Just (WhiteCard One)

increase :: Pile -> Pile
increase (ClassicPile c Nothing) = ClassicPile c (Just One)
increase (ClassicPile c (Just n)) = ClassicPile c (Just $ succ n)

increase (RainbowPile Nothing) = RainbowPile (Just One)
increase (RainbowPile (Just n)) = RainbowPile (Just $ succ n)

increase (WhitePile Nothing) = WhitePile (Just One)
increase (WhitePile (Just n)) = WhitePile (Just $ succ n)

score :: Pile -> Int
score (ClassicPile _ Nothing) = 0
score (ClassicPile _ (Just n)) = rankToInt n

score (RainbowPile Nothing) = 0
score (RainbowPile (Just n)) = rankToInt n

score (WhitePile Nothing) = 0
score (WhitePile (Just n)) = rankToInt n

pileScore = score

instance Show Pile where
  show (ClassicPile c (Just n)) = show c ++ " pile:\t" ++ show n
  show (ClassicPile c Nothing) = show c ++ " pile:\tempty"
  show (RainbowPile (Just n)) = "Rainbow pile:\t" ++ show n
  show (RainbowPile Nothing) = "Rainbow pile:\tempty"
  show (WhitePile (Just n)) = "White pile:\t" ++ show n
  show (WhitePile Nothing) = "White pile:\tempty"

type Discard = Map.Map Card Int

discardCard :: Discard -> Card -> Discard
discardCard d c = Map.adjust (+1) c d

discardAll :: Discard -> [Card] -> Discard
discardAll d [] = d
discardAll d (c:cs) = discardAll (discardCard d c) cs
