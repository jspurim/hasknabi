module State(
  PublicState(..),
  FullState(..),
  Perspective(..),
  makePerspective,
  turn,
  turnsPlayed,
  score,
  isValid,
  calculateMoveEffect,
  calculateMoveEffectFromState,
  applyEffect,
  applyEffectFull,
  applyEffectOnPerspective,
  generateInitialState,
  gameEnded,
  listDiscardedCards,
  blindTo,
  nextPlayerPerspective
) where

import qualified Data.Map as Map
import Data.List
import Data.List.Split
import System.Random
import System.Random.Shuffle

import Cards
import Clues
import Moves
import Players
import Utils
import Variants

data PublicState a = PublicState
                   { discard :: Discard
                   , piles :: [Pile]
                   , deckSize :: Int
                   , lastTurn :: Maybe Int
                   , clues :: Int
                   , strikes :: Int
                   , seats :: Int
                   , history  :: [MoveEffect]
                   , variant :: a
                   } deriving (Show)

turn :: PublicState a -> Int
turn state =
  (length $ history state) `mod` (seats state)

turnsRemaining :: PublicState a -> Maybe Int
turnsRemaining state =
  case lastTurn state of
    Nothing -> Nothing
    Just x -> Just (x-(turnsPlayed state))


turnsPlayed :: PublicState a -> Int
turnsPlayed state = length $ history state

data FullState a = FullState
                 { publicState :: (PublicState a)
                 , hands :: [Hand]
                 , deck ::[Card]
                 } deriving (Show)

data Perspective a = Perspective
                    { seat :: Int
                    , visibleState :: (PublicState a)
                    , partialHands :: [Hand]
                    } deriving (Show)

makePerspective :: FullState a -> Int -> Perspective a
makePerspective state seat=
  blindTo seat $ Perspective {
    seat = seat,
    visibleState = publicState state,
    partialHands = hands state
  }

blindTo ::  Int -> Perspective a -> Perspective a
blindTo s perspective =
  Perspective {
    seat = seat perspective,
    visibleState  = visibleState perspective,
    partialHands = [map (if s == s' then const BlackedOutCard else id) h
      | (s',h) <- zip [0..] hands]
  }
  where
    hands = partialHands perspective

nextPlayerPerspective :: Perspective a -> Perspective a
nextPlayerPerspective perspective =
    (blindTo nextSeat perspective)
  where
    state = visibleState perspective
    nextSeat = (seat perspective +1) `mod` (seats state)

listDiscardedCards :: Variant a =>  PublicState a -> [Card]
listDiscardedCards pstate =
    concat $ map (\x -> replicate (Map.findWithDefault 0 x d) x) cards
  where
    d = discard pstate
    cards = uniqueCards $ variant pstate

generateInitialState :: (Variant a, RandomGen gen) => a -> Int -> gen -> (FullState a)
generateInitialState variant seats gen =
  FullState {
    hands=hands,
    deck=dealtDeck,
    publicState = publicState
  }
  where
    deck = shuffleDeck (generateDeck variant) gen
    handSize = if seats < 4 then 5 else 4
    hands = chunksOf handSize $ take (handSize * seats) deck
    dealtDeck = drop (handSize * seats) deck
    publicState = PublicState {
      discard = generateDiscard variant,
      piles = generatePiles variant,
      deckSize = length dealtDeck,
      clues = 8,
      strikes = 0,
      lastTurn = Nothing,
      seats = seats,
      history = [],
      variant = variant
    }

isValid :: Move -> Int -> [Hand] -> Int -> Bool
isValid  (Discard p) _ hands clues = (clues < 8) &&
  (p `elem` [0..(handSize (length  hands)-1)])
isValid  (Play p) _ hands _ = p `elem` [0..(handSize (length  hands)-1)]
isValid (GiveClue _ _) _ _  0 = False
isValid (GiveClue p clue) turn hands clues =
  (p /= turn ) && (p `elem` [0..(length hands - 1)]) &&
    (any id . matched . makeFullClue clue $ hands  !! p)

calculateMoveEffectFromState :: FullState a -> Move -> MoveEffect
calculateMoveEffectFromState state move =
  calculateMoveEffect t h p c move
  where
    pstate = publicState state
    t = turn pstate
    h = hands state
    p = piles pstate
    c = case deck state of
      (x:xs) -> Just x
      [] -> Nothing

calculateMoveEffect ::  Int -> [Hand] -> [Pile] -> Maybe Card -> Move -> MoveEffect
calculateMoveEffect turn hands piles drawnCard (Play p) =
  case matchedPiles of
    [] -> MissPlayAndDraw p playedCard drawnCard
    (x:_)  -> PlayAndDraw p playedCard x drawnCard
  where
    playedCard = hands !! turn !! p
    matchedPiles =
      [i | (i,pile) <- zip [0..] piles, playedCard `fits` pile]

calculateMoveEffect turn hands _ drawnCard  (Discard p) =
  DiscardAndDraw p discardedCard drawnCard
  where
    discardedCard = hands !! turn !! p

calculateMoveEffect _ hands _ _ (GiveClue p clue) =
  ClueGiven p $ makeFullClue clue $ hands !! p

newDiscard :: MoveEffect -> Discard -> Discard
newDiscard m d =
  case discardedCard of
    Just c -> Map.adjust (\x -> x+1) c d
    Nothing -> d
  where
    discardedCard = case m of
      DiscardAndDraw _ c _ -> Just c
      MissPlayAndDraw _ c _ -> Just c
      _ -> Nothing

newPiles :: MoveEffect -> [Pile] -> [Pile]
newPiles (PlayAndDraw _ _ p _) piles =
  [ if p==p' then increase pile else pile | (p', pile) <- zip [0..] piles ]
newPiles _ piles = piles

newDeckSize :: MoveEffect -> Int -> Int
newDeckSize (ClueGiven _ _) deckSize = deckSize
newDeckSize _ deckSize = max (deckSize-1) 0

newClues :: MoveEffect -> Int -> Int
newClues (ClueGiven _ _) clues = clues -1
newClues (DiscardAndDraw _ _ _) clues = min (clues+1) 8
newClues (PlayAndDraw _ c _ _) clues =
  if rank c == Five then min (clues+1) 8 else clues
newClues _ clues = clues

newStrikes :: MoveEffect -> Int -> Int
newStrikes (MissPlayAndDraw _ _ _) strikes = strikes + 1
newStrikes _ strikes = strikes

newLastTurn :: MoveEffect -> Int -> Maybe Int -> Int -> Maybe Int
newLastTurn (ClueGiven _ _) _ x _ = x
newLastTurn _ 0 (Just t) _ = (Just t)
newLastTurn _ 0 Nothing t = (Just t)
newLastTurn _ _ x _ =  x


applyEffect :: MoveEffect -> PublicState a -> PublicState a
applyEffect effect state =
  PublicState {
      discard = newDiscard effect $ discard state,
      piles = newPiles effect $ piles state,
      deckSize = deckSize',
      lastTurn =
        newLastTurn effect deckSize'
         (lastTurn state) $ (turnsPlayed state)+(seats state)+1,
      clues = newClues effect $ clues state,
      strikes = newStrikes effect $ strikes state,
      seats = seats state,
      history = effect:(history state),
      variant = variant state
    }
  where
    deckSize' = newDeckSize effect $ deckSize state

newHands :: MoveEffect -> [Hand] -> Int-> [Hand]
newHands m hands turn =
  case replacement of
    Just (p, (Just c)) ->
      [if turn==seat then c:(deleteAt p hand) else hand
        | (seat,hand) <- zip [0..] hands]
    Just (p, Nothing) ->
      [if turn==seat then deleteAt p hand else hand
        | (seat,hand) <- zip [0..] hands]
    Nothing -> hands
  where
    deleteAt idx xs = lft ++ rgt
      where (lft, (_:rgt)) = splitAt idx xs
    replacement = case m  of
      (PlayAndDraw p _ _ c) -> Just (p,c)
      (MissPlayAndDraw p _ c) -> Just (p,c)
      (DiscardAndDraw p _ c) -> Just (p,c)
      _ -> Nothing

newDeck :: MoveEffect -> [Card] -> [Card]
newDeck (ClueGiven _ _) deck = deck
newDeck _ (x:xs) = xs
newDeck _ [] = []

applyEffectFull :: MoveEffect -> FullState a -> FullState a
applyEffectFull m state =
  FullState {
    publicState = applyEffect m . publicState $ state,
    hands = newHands m (hands state)  (turn . publicState $ state),
    deck = newDeck m $ deck state
  }

applyEffectOnPerspective :: MoveEffect -> Perspective a -> Perspective a
applyEffectOnPerspective m Perspective{seat=seat,partialHands=hands,visibleState=state}  =
  Perspective {
    visibleState = applyEffect m state,
    partialHands = newHands m hands (turn state),
    seat = seat
  }
score :: PublicState a -> Int
score state =
  if strikes state == 3 then 0 else
    sum $ map pileScore $ piles state

gameEnded :: Variant a => FullState a -> Bool
gameEnded state = any id [
  -- Loss by strikes
  (strikes pstate) == 3,
  -- Loss by discard
  playableCards == [],
  -- Time out, could be win or lose
  (lastTurn pstate) == (Just (turnsPlayed pstate)),
  -- Win!
  score pstate == (maxScore $ variant pstate)
  ]
  where
    pstate = publicState state
    v = variant pstate
    cards = uniqueCards v
    p = piles pstate
    d = discard pstate
    playableCards =
      [c | c <- cards, any (fits c) p , not (outOfPlay d v c)]
