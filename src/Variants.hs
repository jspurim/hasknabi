module Variants(
  Variant (..),
  ClassicVariant (..)
) where

import Data.List
import qualified Data.Map as Map

import Cards
import Clues

class (Show a) => Variant a where
  totalCards :: a -> Int
  uniqueCards ::  a -> [Card]
  cardCounts ::  a -> Map.Map Card Int
  suits :: a -> [Suit]
  generateDeck :: a -> [Card]
  generatePiles :: a -> [Pile]
  generateDiscard :: a -> Map.Map Card Int
  maxScore :: a -> Int
  outOfPlay :: Discard -> a -> Card -> Bool
  allOutOfPlay :: Discard -> a -> [Card]
  allInPlay :: Discard -> a -> [Card]
  clueColors :: a -> [Color]
  clueColors = nub . foldl (++) [] . map clueableColors . suits

data ClassicVariant = ClassicVariant Int deriving (Eq, Show)
instance Variant ClassicVariant where

  totalCards (ClassicVariant colors) = colors*10

  uniqueCards (ClassicVariant colors) =
    [(ClassicCard c n) | n <- enumFrom One , c <- take colors . enumFrom $ Blue]

  cardCounts variant =
    Map.fromList [((ClassicCard c n), count n ) |
      (ClassicCard c n) <- (uniqueCards variant)]
    where
      count One = 3
      count Two = 2
      count Three = 2
      count Four = 2
      count Five = 1
      cout _ = 2

  generateDeck (ClassicVariant colors) =
    [(ClassicCard c n)
      | n <- [One,One,One,Two,Two,Three,Three,Four,Four,Five], c <- take colors . enumFrom $ Blue]

  suits (ClassicVariant colors) =
    [ClassicSuit c | c <- take colors . enumFrom $ Blue]

  generatePiles variant@(ClassicVariant _) = map makePile . suits $ variant

  generateDiscard variant@(ClassicVariant _) =
      Map.fromList [((ClassicCard c n), 0 ) |
        (ClassicCard c n) <- (uniqueCards variant)]

  maxScore (ClassicVariant c) = 5*c

  allOutOfPlay discard variant =
    [c | c <- uniqueCards variant, outOfPlay discard variant c]

  outOfPlay discard variant card =
      Map.lookup card discard == Map.lookup card counts
    where
      counts = cardCounts variant

  allInPlay discard variant =
    [c | c <- uniqueCards variant, not $ outOfPlay discard variant c]
