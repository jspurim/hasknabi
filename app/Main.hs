module Main where

import System.Random

import Bots.PointingBot
import Game
import Moves
import Players
import State
import Variants


players = [ \p -> (NamedPlayer "Alice" (pack . makePointingBot $ p)),
            \p -> (NamedPlayer "Bob" (pack . makePointingBot $ p)),
            \p -> (NamedPlayer "Cathy" (pack . makePointingBot $ p)),
            \p -> (NamedPlayer "Dave" (pack . makePointingBot  $ p))]

initialState = generateInitialState (ClassicVariant 5) 4 (mkStdGen 425)--489
game = makeGame initialState players

displayAndMove ::(Variant a, Player b) => Game a b -> Int -> IO ()
displayAndMove game@(Game state players) t =
  do
    putStrLn $ replicate 80 '='
    putStrLn $ lastMove
    putStrLn $ replicate 80 '='

    putStrLn $ show game
    --foldl (>>) (return ()) $ map (putStrLn . show) players
    if gameFinished game then return () else
      displayAndMove (playOneTurn game) $ t+1

  where
    currentPlayerName = (name $ players !! (mod t $ length players))
    lastPlayerName = (name $ players !! (mod (t-1) $ length players))
    lastMove = case history $ publicState $ state of
      [] -> "Game starts!"
      x:_ -> showMove lastPlayerName (map name players)
        (map show $ suits $ variant $ publicState $ state) x

main :: IO ()
main = do
  displayAndMove game 0
